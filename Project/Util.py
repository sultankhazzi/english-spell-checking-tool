import pickle

'''
    Normalize values in a dictionary between 0 and 1.
'''
def normalize(D):
    base = max(D.values())
    for i in D:
        D[i] /= base

'''
    Gets frequencies of word occuring in generally in English (based on the Kaggle file),
    scaled to 0-1, for increased manageability.
'''
def get_unigram_freq():
    D = dict()
    with open("data/unigram_freq.csv", "r") as f:
        for i in f:
            a, b = i.split(",")
            D[a] = int(b)
    # MinMax scale
    maxi = max(D.values())
    mini = min(D.values())
    for i in D:
        D[i] = (D[i] - mini) / (maxi - mini)
    return D


'''
    Loads custom rules of spell-checking/grammar-checking.
'''
def get_rules():
    D = dict()
    with open("data/rules.txt", "r") as f:
        for i in f:
            a, b = i.split("->")
            a = a.strip()
            b = b.strip()
            D[tuple(a.split(" "))] = b.split(" ")
    return D


'''
    Loads the 4 tables described in the https://aclanthology.org/C90-2036.pdf paper.
'''
def get_tables():
    def load_table(fileName):
        D = dict()
        letters = "abcdefghijklmnopqrstuvwxyz@"
        with open(fileName, "r") as f:
            for letterA in letters:
                line = f.readline().split(" ")
                for j, value in enumerate(line):
                    D[(letterA, letters[j])] = int(value)
        #normalize
        normalize(D)
        return D

    D = dict()
    for fileName in ["delYafterX", "insYafterX", "subXforY"]:
        D[fileName] = load_table("data/" + fileName)
    return D


'''
    Counts frequencies of types of errors, based on http://norvig.com/ngrams/count_1edit.txt. 
'''
def get_norvig_counts():
    D = {"ins": 0, "del": 0, "sub": 0, "trs": 0}
    with open("data/PeterNorvigCounts", "r") as f:
        for i in f:
            i = i.rstrip()
            a, b = i.split("\t")
            b = int(b)
            a1, a2 = a.split("|")
            if len(a1) == 1 and len(a2) == 1:
                D["sub"] += b
            elif len(a1) == 1 and len(a2) == 2:
                D["del"] += b
            elif len(a1) == 2 and len(a2) == 1:
                D["ins"] += b
            else:
                D["trs"] += b
    # normalize
    normalize(D)
    return D


'''
    Parse and save ngrams and their frequencies from Wikipedia dataset into two pickle files.
    NOTE: This only runs one time to only create/update these parsed files.
'''
def get_ngram_freqs():
    unigrams = dict()
    with open("data/wp_1gram.txt", encoding='utf-8') as f:
        for i in f:
            i = i.split()
            cnt = int(i[0])
            if cnt > 1 and len(i) == 2:  # Take only ngrams appearing more than once
                unigrams[i[1].lower()] = cnt
    print("unigrams loaded")
    two_grams = dict()
    with open("data/wp_2gram.txt", encoding='utf-8') as f:
        for i in f:
            i = i.split()
            cnt = int(i[0])
            if cnt > 1 and len(i) == 3:  # Take only ngrams appearing more than once
                two_grams[(i[1].lower(), i[2].lower())] = cnt

    file = open("data/wp_1gram_parsed.pkl", "wb")
    pickle.dump(unigrams, file)
    file.close()

    file = open("data/wp_2gram_parsed.pkl", "wb")
    pickle.dump(two_grams, file)
    file.close()


'''
    Load ngrams and their frequencies from parsed pickle files.
    NOTE: This runs everytime the program executes.
'''
def load_ngrams_freqs():
    file = open("data/wp_1gram_parsed.pkl", "rb")
    unigrams = pickle.load(file)
    print("unigrams loaded")
    file.close()

    # file = open("data/wp_2gram_parsed.pkl", "rb")
    # two_grams = pickle.load(file)
    # print("two grams loaded")
    # file.close()
    print("Not loading 2grams")
    two_grams = {".":1}

    normalize(unigrams)
    normalize(two_grams)
    return unigrams, two_grams


if __name__ == "__main__":
    import time

    # print(get_unigram_freq())
    # print(get_rules())
    # print(get_tables())
    # print(get_norvig_counts())
    start = time.time()
    # print(get_ngram_freqs())
    print(load_ngrams_freqs())
    end = time.time()
    print(f"Runtime of the program is {end - start}")