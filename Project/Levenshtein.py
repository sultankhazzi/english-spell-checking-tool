from Trie import Trie


class TrieLevenshtein:
    def __init__(self, trie):
        self.trie = trie

    '''
        Search the trie for the closest matching word.
        Start with 0 distance, then 1, then 2 etc., until a word(s) is found.
    '''

    def iterative_search(self, X):
        cost = 0
        res = []
        while len(res) == 0:
            res = self.search(X, cost)
            cost += 1
        return res

    '''
        Search the trie for matching words, up to the maximum distance.
    '''

    def search(self, X, maximum_distance):
        X = X.lower()
        res = []
        initial_row = ([(i, ["DEL"] * i) for i in range(len(X) + 1)])
        for letter in self.trie.root:
            self.searchRecursive(self.trie.root[letter], letter, initial_row, X, res, maximum_distance)
        return res

    '''
        Recursive dynamic programming function which searches through the trie, calculates distances between nearby words, and
        returns words similar up to the maximum distance. Distance is defined as any deviation (insertion, deletion, replacement) of 
        characters between the two words.

        Note: tries to convert potential correct word to wrong one, in order to see how distant they are.
        Therefore, insertion cost of 1 means that one insertion is needed to convert a potential correct word to the current wrong one.

        Note: for example, DELe/n means that, starting from the correct word, e was deleted after n to reach the current wrong word. 
              aeroplane -> aeroplan 
    '''

    def searchRecursive(self, node, letter, pre_row, X, results, maximum_distance):
        curr_row = [(pre_row[0][0] + 1, ["INS"])]
        columns = len(X) + 1

        for column in range(1, columns):

            insertion_cost = (curr_row[column - 1][0] + 1, curr_row[column - 1][1] + [f"INS{letter}/{X[column - 1]}"])
            deletion_cost = (pre_row[column][0] + 1, pre_row[column][1] + [f"DEL{letter}/{X[column - 1]}"])

            if X[column - 1] != letter:
                replace_cost = (pre_row[column - 1][0] + 1, pre_row[column - 1][1] + [f"SUB{letter}/{X[column - 1]}"])
            else:
                replace_cost = (pre_row[column - 1][0], pre_row[column - 1][1])

            curr_row.append(min(insertion_cost, deletion_cost, replace_cost, key=lambda x: x[0]))

        if curr_row[-1][0] <= maximum_distance and self.trie.is_word(node):
            results.append((node["#"], curr_row[-1]))

        if min(curr_row, key=lambda x: x[0])[0] <= maximum_distance:
            if self.trie.is_end(node):
                return
            for letter in node:
                self.searchRecursive(node[letter], letter, curr_row, X, results, maximum_distance)


if __name__ == "__main__":
    tree1 = Trie()  # Create the tree object.

    with open("data/ukenglish.txt") as file:
        for line in file:
            tree1.insert_word(line.rstrip())

    results = TrieLevenshtein(tree1).iterative_search("aeroplannn")
    print(results)