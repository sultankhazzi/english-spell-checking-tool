'''
Title:Implement Trie (Prefix Tree) in Python
Author: Arnab Chakraborty
Date: 2020
Code Version: 1.0
Availablity: https://www.tutorialspoint.com/implement-trie-prefix-tree-in-python
'''


# Trie tree class
class Trie(object):
    def __init__(self):
        self.root = {}

    # To add a word into the tree.
    def insert_word(self, word):
        word = word.lower()
        current = self.root
        for char in word:
            if char not in current:
                current[char] = {}
            current = current[char]
        current['#'] = word

    # To Display the latest tree.
    def display_tree(self):
        return self.root

    '''
        Returns if the current node represents a word or not.
        This node may still have children if there are words which can be made on top of this word.
    '''
    @staticmethod
    def is_word(node):
        if '#' in node:
            return True
        return False

    '''
        Returns if the current node represents an end or not.
        This node has no children. An end node is a child node of a node which represents a full word.
        In the end node, the entire represented word of the parent is stored.
    '''
    @staticmethod
    def is_end(node):
        if type(node) == str:
            return True
        return False

    # To search for a word in the tree. Returns True if the word exists, Otherwise return False.
    def search_word(self, word):
        current = self.root
        for char in word:
            if char not in current:
                return False
            current = current[char]
        return '#' in current

if __name__ == "__main__":
    tree = Trie()  # Create the tree object.

    # Loading the list of words into the tree.
    with open("data/ukenglish.txt") as file:  # Enconding type is ANSI (Knew from notepad when Saving as the file).
        for line in file:
            tree.insert_word(line.rstrip())  # Add each word into the tree.

    print(tree.display_tree())