from CandidateEvaluator import CandidateEvaluator
from Trie import Trie
from Levenshtein import TrieLevenshtein
from DeletedInterpolation import Deletedinterpolation
import regex as re

'''
    Pipeline class for supporting the entire grammar and spellchecking algorithm.
    Called by the GUI to make spellchecking decisions.
'''


class Pipeline:
    def __init__(self, dl: Deletedinterpolation):
        self.dl = dl

    '''
        Generate candidates for each word in the text.
    '''

    def proof(self, text):
        word_suggestions = dict()  # Multiple suggestions per word possible
        sent_index = 0
        for sentence in re.split("\.|!|;|\?|\n|,", text):
            sentence = sentence.strip()
            if len(sentence) > 0:
                if sent_index not in word_suggestions:
                    word_suggestions[sent_index] = list()
                for word in sentence.split(" "):
                    word_suggestions[sent_index].append(self.dl.cg.get_N_candidates(word.lower(), 5))
                sent_index += 1
        return word_suggestions

    '''
        Match string a and b so that b is capitalized if a is capitalized.
    '''

    @staticmethod
    def case_match(a, b):
        if a == 'i':
            return 'I'
        if a[0].isupper():
            return b.capitalize()
        return b.lower()

    '''
        Based on previously generated candidates, match top 5 candidates of each word to the original word in the text.
    '''

    def propose_per_word(self, text, word_suggestions: dict):
        top_candidates = []
        sent_index = 0
        for sentence in re.split("\.|!|;|\?|\n|,", text):
            sentence = sentence.strip()
            if len(sentence) > 0:
                for i, word in enumerate(sentence.split(" ")):
                    candidates = word_suggestions[sent_index][i]
                    top_candidates_ = []
                    for candidate in candidates:
                        top_candidates_.append((candidate, self.dl.cg.evaluate_candidate(candidate)))
                    top_candidates_ = sorted(top_candidates_, key=lambda x: -x[1])
                    top_candidates_ = [Pipeline.case_match(word, i[0][0]) for i in top_candidates_[:5]]  # Select top 5
                    for candidate in candidates:
                        # High priority candidate from the grammar check
                        if len(candidate) == 3 and Pipeline.case_match(word, candidate[0]) not in top_candidates_:
                            top_candidates_.append(Pipeline.case_match(word, candidate[0]))
                    top_candidates.append((word, top_candidates_))
                sent_index += 1
        return top_candidates

    '''
        Run the pipeline: find word & sentence suggestions, and match them to the original words.
    '''

    def run(self, text):
        text = self.dl.apply_rules(text)
        word_suggestions = self.proof(text)
        grammared_proposals = []
        sent_index = 0
        for sentence in re.split("\.|!|;|\?|\n|,", text):
            sentence = sentence.strip()
            if len(sentence) > 0:
                grammared_proposal = self.dl.get_best_sentence(sentence, word_suggestions, sent_index).split()
                # Fix casing
                Z = sentence.split()
                for i in range(len(Z)):
                    grammared_proposal[i] = Pipeline.case_match(Z[i], grammared_proposal[i])

                if ' '.join(grammared_proposal) != sentence:
                    # Fix punctuation (find relevant sentence and see what ending punctuation it has)
                    cnt = 0
                    i = 0
                    while (cnt < sent_index):
                        if text[i] in "\.|!|;|\?|,":
                            cnt += 1
                        i += 1
                    text_ = text[i:]
                    temp_text = text_[text_.find(sentence): text_.find(sentence) + len(sentence) + 1]
                    temp_text = temp_text.replace(sentence, " ".join(grammared_proposal))

                    grammared_proposals.append(temp_text)
                else:
                    grammared_proposals.append(sentence + '&')  # special tag for correct sentences

                sent_index += 1
        for i, proposal in enumerate(grammared_proposals):
            grammared_proposals[i] = re.sub("\n+", "", proposal)

        proposals = self.propose_per_word(text, word_suggestions)
        return proposals, grammared_proposals


if __name__ == "__main__":
    tree = Trie()
    with open("data/ukenglish.txt") as file:
        for line in file:
            tree.insert_word(line.rstrip())
    dl = Deletedinterpolation(CandidateEvaluator(TrieLevenshtein(tree)))
    pipe = Pipeline(dl)
    pipe.run("I has an aeroppplanes. They fly verry high!")