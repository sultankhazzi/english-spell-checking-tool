from Levenshtein import TrieLevenshtein
import Util
from Trie import Trie
import numpy as np

'''
    Generates candidates for each misspelled word, and evaluates them.
'''
class CandidateEvaluator:
    def __init__(self, lieh_trie : TrieLevenshtein):
        self.lieh_trie = lieh_trie
        self.unigram_freqs = Util.get_unigram_freq()
        self.tables = Util.get_tables()
        self.norvig_counts = Util.get_norvig_counts()
    '''
        Generate candidates from the Levenshtein's trie.
    '''
    def get_candidates(self, word):
        return self.lieh_trie.iterative_search(word)

    '''
        Generate N candidates from the Levenshtein's trie.
        If the original word exists int the trie, return only the word.
    '''
    def get_N_candidates(self, word, N):
        candidates = self.lieh_trie.search(word, 0)
        dist = 1
        if len(candidates) == 1: #original word exists, so it is probably not wrong
            return candidates
        while len(candidates) < N:
            candidates.extend(self.lieh_trie.search(word, dist))
            #remove duplicates
            seen = []
            candidates_ = []
            for i in range(len(candidates)):
                if candidates[i] not in seen:
                    seen.append(candidates[i])
                    candidates_.append(candidates[i])
            candidates = candidates_
            dist += 1
            if dist==5: #For words distant more than 4, there is no point to keep searching..
                break
        if len(candidates) == 0: #Nothing found
            return [(word,(0,[]))]
        return candidates

    '''
        Score a candidate word to see how likely it is.
    '''
    def evaluate_candidate(self, candidate):
        SCORES = [100,0.01,1]
        MAP = {"DEL":"delYafterX", "INS":"insYafterX", "SUB":"subXforY"}
        if len(candidate) == 2:
            candidate, ops = candidate
        else:
            candidate, ops, _ = candidate
        if candidate in self.unigram_freqs:
            SCORES[0] *= self.unigram_freqs[candidate]
        else:
            SCORES[0] = np.average(list(self.unigram_freqs.values()))
        for operation in ops[1]:
            type = operation[:3]
            if len(operation) > 3:
                x, y = operation[3:].split("/")
                try:
                    SCORES[2] *= self.tables[MAP[type]][(x, y)]
                except:
                    SCORES[2] *= np.average(list(self.tables[MAP[type]].values()))
            else:
                SCORES[2] *= np.average(list(self.tables[MAP[type]].values()))
            SCORES[1] *= self.norvig_counts[type.lower()]

        return sum(SCORES) + 100*(10-ops[0]) #consider scoring and number of edits

if __name__ == "__main__":
    tree = Trie()
    with open("data/ukenglish.txt") as file:
        for line in file:
            tree.insert_word(line.rstrip())
    cg = CandidateEvaluator(TrieLevenshtein(tree))
    candidates = cg.get_N_candidates("tthis", 5)
    for candidate in candidates:
        print(candidate, end="--")
        print(cg.evaluate_candidate(candidate))