from Pipeline import Pipeline

import tkinter as tk
from tkinter import *
import tkinter.scrolledtext as scrolledtext

from CandidateEvaluator import CandidateEvaluator
from Trie import Trie
from Levenshtein import TrieLevenshtein
from DeletedInterpolation import Deletedinterpolation
import regex as re
import time
'''
    GUI class
'''
class GUI(tk.Tk):

    def __init__(self, text = False):
        super(GUI, self).__init__()
        self.title("Graphical User Interface")
        self.minsize(1200, 600)

        self.language = tk.StringVar()
        self.language.set("UK")
        self.layout()
        self.make_pipe("ukenglish.txt")
    '''
        Create pipeline for the spellchecking algorithm.
    '''
    def make_pipe(self, filename):
        tree = Trie()
        with open(f"data/{filename}") as file:
            for line in file:
                tree.insert_word(line.rstrip())
        pipe = Pipeline(Deletedinterpolation(CandidateEvaluator(TrieLevenshtein(tree))))
        self.pipe = pipe

    '''
        Create layout for the GUI.
    '''
    def layout(self):
        menu = tk.Menu()
        self.config(menu=menu)

        canvas = tk.Canvas(height=800, width=900)
        canvas.pack()
        self.frame = tk.Frame(bg='#AFAFAF', bd=1)
        self.frame.place(relx=0.5, rely=0.1, relwidth=0.75, relheight=0.8, anchor='n')

        label_1 = tk.Label(self.frame, text="Type in this box:", bg='#AFAFAF',
                            fg="black", font="none 10 normal")
        label_1.place(relx=0.1, rely=0.05)

        textContainer_1 = tk.Frame(self.frame, relief="sunken")
        self.text_1 = tk.Text(textContainer_1, wrap="none", borderwidth=0)
        textVsb = tk.Scrollbar(textContainer_1, orient="vertical", command=self.text_1.yview)
        textHsb = tk.Scrollbar(textContainer_1, orient="horizontal", command=self.text_1.xview)
        self.text_1.configure(yscrollcommand=textVsb.set, xscrollcommand=textHsb.set)
        self.text_1.grid(row=0, column=0, sticky="nsew")
        self.text_1.focus()
        textVsb.grid(row=0, column=1, sticky="ns")
        textHsb.grid(row=1, column=0, sticky="ew")

        textContainer_1.grid_rowconfigure(0, weight=1)
        textContainer_1.grid_columnconfigure(0, weight=1)

        textContainer_1.pack(fill="both", expand=True)
        textContainer_1.place(relx = 0.1, rely = 0.1, relwidth = 0.35, relheight = 0.4)


        self.popup_menu = tk.Menu(self, tearoff=0, background='#1c1b1a',
                                  fg='white', activebackground='#534c5c',
                                  activeforeground='Yellow')
        self.text_1.tag_bind("sel", '<Button-3>', self.popup)

        label_2 = tk.Label(self.frame, text="Original Input:", bg='#AFAFAF',
                            fg="black", font="none 10 normal")
        label_2.place(relx=0.1, rely=0.51)

        textContainer_2 = tk.Frame(self.frame, relief="sunken")
        self.text_2= tk.Text(textContainer_2, wrap="none", borderwidth=0)
        textVsb = tk.Scrollbar(textContainer_2, orient="vertical", command=self.text_2.yview)
        textHsb = tk.Scrollbar(textContainer_2, orient="horizontal", command=self.text_2.xview)
        self.text_2.configure(yscrollcommand=textVsb.set, xscrollcommand=textHsb.set)
        self.text_2.grid(row=0, column=0, sticky="nsew")
        textVsb.grid(row=0, column=1, sticky="ns")
        textHsb.grid(row=1, column=0, sticky="ew")

        textContainer_2.grid_rowconfigure(0, weight=1)
        textContainer_2.grid_columnconfigure(0, weight=1)

        textContainer_2.pack(fill="both", expand=True)
        textContainer_2.place(relx=0.1, rely=0.51, relwidth=0.35, relheight=0.4)
        self.text_2.configure(state='disabled')

        label_3 = tk.Label(self.frame, text="Proposed sentences:", bg='#AFAFAF',
                            fg="black", font="none 10 normal")
        label_3.place(relx=0.51, rely=0.05)

        textContainer_3 = tk.Frame(self.frame, relief="sunken")
        self.text_3 = tk.Text(textContainer_3, wrap="none", borderwidth=0)
        textVsb = tk.Scrollbar(textContainer_3, orient="vertical", command=self.text_3.yview)
        textHsb = tk.Scrollbar(textContainer_3, orient="horizontal", command=self.text_3.xview)
        self.text_3.configure(yscrollcommand=textVsb.set, xscrollcommand=textHsb.set)
        self.text_3.grid(row=0, column=0, sticky="nsew")
        textVsb.grid(row=0, column=1, sticky="ns")
        textHsb.grid(row=1, column=0, sticky="ew")

        textContainer_3.grid_rowconfigure(0, weight=1)
        textContainer_3.grid_columnconfigure(0, weight=1)

        textContainer_3.pack(fill="both", expand=True)
        textContainer_3.place(relx=0.51, rely=0.1, relwidth=0.35, relheight=0.4)
        self.text_3.configure(state='disabled')


        self.text_3.tag_bind("sel", '<Button-3>', self.popup3)
        self.text_3.tag_config('good_sentence', foreground="green")
        self.text_3.configure(state='disabled')

        Button1 = tk.Button(self.frame, text="Submit", width=7, command=self.submit)
        Button1.place(relx=0.72, rely=0.05)

        self.Button2 = tk.Button(self.frame, textvariable=self.language, width=3, command=self.lang)
        self.Button2.place(relx=0.81, rely=0.05)

        Button3 = tk.Button(self.frame, text="Auto", width=7, command=self.auto_grammar)
        Button3.place(relx=0.86, rely=0.05)


        instruction_1 = tk.Label(self.frame, text="Instructions:", anchor='w',
                         fg="black", font="none 10 bold")
        instruction_1.place(relx=0.51, rely=0.51, relwidth=0.42)
        instruction_2 = tk.Label(self.frame, text="1. Enter text in the given area.",
                         anchor='w')
        instruction_2.place(relx=0.51, rely=0.54, relwidth=0.42)
        instruction_3 = tk.Label(self.frame, text="2. Press the submit button.",
                         anchor='w')
        instruction_3.place(relx=0.51, rely=0.57, relwidth=0.42)
        instruction_4 = tk.Label(self.frame, text="3. Double-click a highlighted word to select it.",
                         anchor='w')
        instruction_4.place(relx=0.51, rely=0.60, relwidth=0.42)
        instruction_5 = tk.Label(self.frame, text="4. Right-click the selected error word.",
                         anchor='w')
        instruction_5.place(relx=0.51, rely=0.63, relwidth=0.42)
        instruction_6 = tk.Label(self.frame, text="5. Choose one candidate correction word.",
                         anchor='w')
        instruction_6.place(relx=0.51, rely=0.66, relwidth=0.42)
        instruction_7 = tk.Label(self.frame, text="6. Alternatively, highlight a proposed sentence",
                                 anchor='w')
        instruction_7.place(relx=0.51, rely=0.69, relwidth=0.42)
        instruction_7_2 = tk.Label(self.frame,
                                 text="    and right click to choose it.",
                                 anchor='w')
        instruction_7_2.place(relx=0.51, rely=0.72, relwidth=0.42)
        instruction_8 = tk.Label(self.frame, text=" - Choose language (UK/US) by toggling the button!",
                                 anchor='w')
        instruction_8.place(relx=0.51, rely=0.75, relwidth=0.42)
        instruction_9 = tk.Label(self.frame, text=" - Click Auto to select all grammar suggestions!",
                                 anchor='w')
        instruction_9.place(relx=0.51, rely=0.78, relwidth=0.42)

    '''
        Automatically copy all grammar suggestions, when button is clicked.
    '''
    def auto_grammar(self):
        self.text_1.delete('1.0', tk.END)
        self.text_1.insert('1.0', self.text_3.get('1.0', tk.END))
    '''
        Change used language.
    '''
    def lang(self):
        if self.language.get() == "UK":
            self.language.set("US")
            self.make_pipe("usenglish.txt")
        else:
            self.language.set("UK")
            self.make_pipe("ukenglish.txt")
    '''
        Check if text is selected, and flag the selected text.
    '''
    def text_selected(self):
        if self.non_words:
            self.selection_ind = self.text_1.tag_ranges(tk.SEL)
            if self.selection_ind:
                return True
            else:
                return False
        else:
            return False

    '''
        Function for enabling the selection of one of the alternative correction options.
    '''
    def choose_correction(self, correction):
        self.text_1.delete(*self.selection_ind)
        self.text_1.insert(tk.INSERT, correction)

    '''
        Function to handle the popup right-click.
    '''
    def popup(self, event):

        if self.text_selected():
            selected = self.text_1.get(*self.selection_ind)

            if selected in self.non_words:
                try:

                    nd = len(self.non_word_to_candidate[selected])
                    self.popup_menu.delete(0, nd + 3)

                    c = self.non_word_to_candidate[selected]

                    if nd == 0:
                        self.popup_menu.add_command(label="No suggestions.")
                        print("No suggestions.")

                    if nd > 0:
                        self.popup_menu.add_command(label=f"{selected} | {c[0]}",
                                                    command=lambda: self.choose_correction(c[0]))
                        self.popup_menu.add_separator()

                    if nd > 1:
                        self.popup_menu.add_command(label=f"{selected} | {c[1]}",
                                                    command=lambda: self.choose_correction(c[1]))
                    if nd > 2:
                        self.popup_menu.add_command(label=f"{selected} | {c[2]}",
                                                    command=lambda: self.choose_correction(c[2]))
                    if nd > 3:
                        self.popup_menu.add_command(label=f"{selected} | {c[3]}",
                                                    command=lambda: self.choose_correction(c[3]))
                    if nd > 4:
                        self.popup_menu.add_command(label=f"{selected} | {c[4]}",
                                                    command=lambda: self.choose_correction(c[4]))
                    if nd > 5:
                        self.popup_menu.add_command(label=f"{selected} | {c[5]}",
                                                    command=lambda: self.choose_correction(c[5]))

                    self.popup_menu.tk_popup(event.x_root, event.y_root)
                finally:
                    self.popup_menu.grab_release()

            else:
                pass
        else:
            pass

    '''
        Choose a sentence and replace it in the main box.
    '''
    def choose_sentence(self, sentence):
        sentence_index = int(self.selection_ind[0].split(".")[0]) - 1
        sentences = self.text_1.get("1.0", tk.END)
        sent_index = 0
        row = 1
        column = 0
        for j, letter in enumerate(sentences):
            if letter in "\.|!|;|\?|,":
                sent_index += 1
            if letter == "\n":
                row += 1
                column = 0
                continue
            if sent_index == sentence_index:
                jj = j
                while (sentences[jj] in " |\.|!|;|\?|,|\n"): #omit previous sentences's end punctuation. Omit new sentence's leading space and newline
                    if sentences[jj] == "\n":
                        row += 1
                        column = 0
                        jj+=1
                        continue
                    column += 1
                    jj += 1

                #find original sentence length
                sentences_ = re.split("\.|!|;|\?|\n|,", sentences)
                sentences_ = [i.strip() for i in sentences_ if len(i)>0 and i!=" "]
                length = len(sentences_[sent_index]) + 1
                self.selection_ind = (str(row) + "." + str(column), str(row) + "." + str(column + length))
                self.text_1.delete(*self.selection_ind)
                self.text_1.insert(str(row) + "." + str(column), sentence)
                break
            column += 1

    '''
        Handle sentence right-click selection.
    '''
    def popup3(self, _):
        self.selection_ind = self.text_3.tag_ranges(tk.SEL)
        if self.selection_ind:
            temp = self.selection_ind[0].string.split(".")[0]
            self.selection_ind = (temp+".0", temp+".99")
            selected_sentence = self.text_3.get(*self.selection_ind)
            self.choose_sentence(selected_sentence)
        else:
            pass

    '''
        Submitting (pushing the submit button) activates this function.
        All words are checked and erroneous ones are highlighted in red.
    '''
    def submit(self, test_time = True):
        self.non_words = []
        self.non_word_to_candidate = dict()
        self.text_2.configure(state='normal')
        self.text_2.delete('1.0', tk.END)

        user_input = self.text_1.get('1.0', 'end-1c')

        # Fix sentence spacing & punctutation & lowercase i
        user_input = re.sub(" ([.|!|;|\?|,])+", r"\1", user_input)
        user_input = re.sub(r"([.|!|;|\?|,])+", r"\1 ", user_input)
        user_input = re.sub(r" +", r" ", user_input)
        user_input = re.sub(r"([a-zA-Z0-9_]) *\n", r"\1.\n", user_input)
        user_input = re.sub(r"([^a-zA-Z0-9_])i([^a-zA-Z0-9_])", r"\1I\2", user_input)
        user_input += "\n"
        self.text_1.delete('1.0', tk.END)
        self.text_1.insert(tk.INSERT, user_input)

        #Spellcheck
        if test_time == True:
            t0 = time.process_time()
        proposals, grammared_proposals = self.pipe.run(user_input)
        for proposal in proposals:
            if not proposal[0] == proposal[1][0]: #different original word and candidate
                self.non_words.append(proposal[0])
                self.non_word_to_candidate[proposal[0]] = proposal[1]

        self.text_1.tag_config("red_tag", foreground="red")
        for err in self.non_words:
            offset = '+%dc' % len(err)
            pos_start = self.text_1.search(err, '1.0', tk.END)
            while pos_start:
                pos_end = pos_start + offset
                self.text_1.tag_add("red_tag", pos_start, pos_end)
                pos_start = self.text_1.search(err, pos_end, tk.END)

        self.text_2.insert(tk.INSERT, user_input)
        self.text_2.configure(state='disabled')

        self.text_3.configure(state='normal')
        self.text_3.delete('1.0', tk.END)
        for proposal in grammared_proposals:
            if proposal[-1] == '&': #correct sentence
                self.text_3.insert(tk.INSERT, proposal[:-1] + "\n", 'good_sentence')
            else:
                self.text_3.insert(tk.INSERT, proposal + "\n")
        self.text_3.configure(state='disabled')
        if test_time:
            runtime = time.process_time() - t0
            print('Running time: ', runtime, ' seconds')
            return runtime

if __name__ == "__main__":
    sgui = GUI()
    sgui.mainloop()