from GUI import GUI
import tkinter as tk
import matplotlib.pyplot as plt

'''
    Runs the stress test files through the program, and plots execution time for each.
'''
if __name__ == "__main__":
    runtimes = []
    no_words = []
    sgui = GUI()
    for i in range(9,29):
        with open(f'testing/stress_test/test{i}', 'r') as f:
            text = f.read()
            print(f'file_{i}, no_words={i-8}')
            sgui.text_1.insert(tk.INSERT, text)

            runtimes.append(sgui.submit())
            no_words.append(i-8)

            sgui.text_1.delete('1.0', tk.END)
    plt.plot(no_words, runtimes)
    plt.xlabel("Number of incorrect words")
    plt.ylabel('Execution time (s)')
    plt.show()
    sgui.mainloop()