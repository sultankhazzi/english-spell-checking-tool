from GUI import GUI
import tkinter as tk

'''
    Runs the model against the 100 words from https://www.englishclub.com/spelling/misspellings.htm,
    and compares word suggestions to the true solutions.
'''
if __name__ == "__main__":
    n = int(input("Test on 100 words [1] or Mitton's words [2]?"))
    if n==1:
        print("Testing on the 100 words...")
        words = []
        sgui = GUI()
        with open('testing/quality_test/100words','r') as f:
            correct = True
            cnt = 0
            for i in f:
                if i.startswith('#'):
                    correct = False
                    continue
                if correct:
                    words.append([i.strip()])
                else:
                    i = i.split(',')
                    i = [j.strip() for j in i]
                    words[cnt].extend(i)
                    cnt+=1
        scores = []
        for lista in words:
            true_word = lista[0]
            false_words = lista[1:]
            for false_word in false_words:
                #print(false_word)
                sgui.text_1.insert(tk.INSERT, false_word)
                sgui.submit(False)
                if false_word in sgui.non_word_to_candidate:
                    if true_word == sgui.non_word_to_candidate[false_word][0]:
                        scores.append(1)
                    elif true_word in sgui.non_word_to_candidate[false_word]:
                        scores.append(0.5)
                    else:
                        scores.append(-0.5)
                else:
                    scores.append(0)
                sgui.text_1.delete('1.0', tk.END)

        #print scores
        print(scores)
        D = {-0.5:0, 0:0, 0.5:0, 1:0}
        for i in scores:
            D[i] +=1
        print(D)
    else:
        print("Testing on Mitton's words...")
        sgui = GUI()
        scores = []
        with open('testing/quality_test/Mitton words', 'r') as f:
            for line in f:
                if line.startswith("$"):
                    true_word = line[1:].strip()
                else:
                    false_word = line.strip()

                    sgui.text_1.insert(tk.INSERT, false_word)
                    sgui.submit(False)
                    if false_word in sgui.non_word_to_candidate:
                        if true_word == sgui.non_word_to_candidate[false_word][0]:
                            scores.append(1)
                        elif true_word in sgui.non_word_to_candidate[false_word]:
                            scores.append(0.5)
                        else:
                            scores.append(-0.5)
                    else:
                        scores.append(0)
                    sgui.text_1.delete('1.0', tk.END)
        print(scores)
        D = {-0.5: 0, 0: 0, 0.5: 0, 1: 0}
        for i in scores:
            D[i] += 1
        print(D)