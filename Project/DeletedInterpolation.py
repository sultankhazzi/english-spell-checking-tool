import Util
from CandidateEvaluator import CandidateEvaluator
from Trie import Trie
from Levenshtein import TrieLevenshtein

'''
    Performs the deleted interpolation method on a sentence. Augmented with individual word scoring.
    For each given sentence, generates best candidates for each word of that sentence, then scores the probability of
    a new sentence with only one word replaced by a candidate at a time.
'''
class Deletedinterpolation:
    def __init__(self, cg : CandidateEvaluator):
        self.unigrams, self.two_grams = Util.load_ngrams_freqs()
        self.cg = cg
        self.rules = Util.get_rules()

    '''
        Applies the custom rules to sentences
    '''
    def apply_rules(self, sentence):
        for rule in self.rules.keys():
            phrase = " ".join(rule)
            if phrase in sentence:
                sentence = sentence.replace(phrase, " ".join(self.rules[rule]))
        return sentence
    '''
        Calculates sentence probability.
    '''
    def calculate_sentence_probability(self, sentence : list):
        LAMBDAS = [0.5e7, 0.5e7]
        probability = 0
        for i in range(1, len(sentence)):
            curr_word = sentence[i]
            prev_word = sentence[i-1]
            try: #sometimes words are not in dictionary
                probability += LAMBDAS[0]*self.unigrams[curr_word]
            except:
                probability += 0
            try: #sometimes words are not in dictionary
                probability += LAMBDAS[1]*self.two_grams[(prev_word, curr_word)]
            except:
                probability += 0
        return probability

    '''
        Convert a single sentence into multiple ones, one for each combination of candidate words.
    '''
    def explode(self, sentence: list):
        candidate_sets = []
        candidate_sentences = [[]]
        for word in sentence:
            candidate_sets.append(self.cg.get_N_candidates(word, 5))
        for i in range(len(candidate_sets)):
            Z = len(candidate_sentences)
            for j in candidate_sets[i]:
                for k in range(Z):
                    candidate_sentences.append(candidate_sentences[k] + [j])
            for k in range(Z):
                candidate_sentences.remove(candidate_sentences[k])
        return candidate_sentences


    '''
        Generates multiple sentences from the original one, and returns the most likely sentence.
        If a rule applies for the sentence, then the rule is first applied before further processing.
        Append grammar-based word suggestions to normal word suggestions.
    '''
    def get_best_sentence(self, original_sentence : str, word_suggestions : dict, sent_index : int):
        original_sentence = original_sentence.lower()
        best_sentence, highest_prob = None, -1
        original_sentence = original_sentence.split(" ")

        candidate_sentences = self.explode(original_sentence)
        for sentence in candidate_sentences:
            del_interpol_score = self.calculate_sentence_probability([z[0] for z in sentence])
            individual_word_score = 0
            for word in sentence:
                individual_word_score += self.cg.evaluate_candidate(word)
            total_score = del_interpol_score + individual_word_score
            if total_score > highest_prob:
                highest_prob = total_score
                best_sentence = sentence
        if best_sentence == None:
            return "No suggestion."
        for i, z in enumerate(best_sentence):
            if z not in word_suggestions[sent_index][i]:
                z = (z[0], z[1], '!') #Ensure it gets passed
            else:
                word_suggestions[sent_index][i].remove(z)
                z = (z[0], z[1], '!') #Ensure it gets passed
            word_suggestions[sent_index][i].append(z)

        best_sentence = [z[0] for z in best_sentence]
        return ' '.join(best_sentence)

if __name__ == "__main__":
    tree = Trie()
    with open("data/ukenglish.txt") as file:
        for line in file:
            tree.insert_word(line.rstrip())
    cg = CandidateEvaluator(TrieLevenshtein(tree))
    dl = Deletedinterpolation(cg)

    print(dl.get_best_sentence("I has an aeroppplanes"))